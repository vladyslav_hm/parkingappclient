﻿using ParkingAppClient.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParkingAppClient.Models.UserCommandModels
{
    public class Commands
    {
        public List<MenuItem<string, string>> menuItems = new List<MenuItem<string, string>>();

        public Commands(ParkingController parkingController)
        {
            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetBalance,
                    Description = "Get Balance",
                    HotKey = 1
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetProfit,
                    Description = "Get profit in the last minute",
                    HotKey = 2
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetPlaceStatus,
                    Description = "Get PlaceStatus",
                    HotKey = 3
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetTransactions,
                    Description = "Get transactions in the last minute",
                    HotKey = 4
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetTransactionsHistory,
                    Description = "Get all transaction history",
                    HotKey = 5
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetAllVehicleOnParking,
                    Description = "Get all vehicles on parking",
                    HotKey = 6
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = parkingController.GetAllAvailableVehicle,
                    MakerFuncHandler = parkingController.AddVehicleInParking,
                    Description = "Add vehicle on parking",
                    HotKey = 7
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = new Func<string>(() => "Input id Vehicle to delete from parking"),
                    MakerFuncHandler = parkingController.DeleteVehicleFromParking,
                    Description = "Delete vehicle from parking",
                    HotKey = 8
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = new Func<string>(() => "Input id Vehicle to Topup balance. InputFormat: {GUID};{Value}"),
                    MakerFuncHandler = parkingController.TopUpVehicleBalance,
                    Description = "Topup balance selected vehicle",
                    HotKey = 9
                });

            menuItems.Add(
                new MenuItem<string, string>()
                {
                    GetterFuncHandler = new Func<string>(() => string.Join("\n", menuItems.Select(s =>
                        new string($"{ s.HotKey } - { s.Description }")))),
                    Description = "Get Help",
                    HotKey = 0
                });
        }
    }
}
