﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingAppClient.Models.UserCommandModels
{
    public class MenuItem<T,V>
    {
        public string Description { get; set; }
        public byte HotKey { get; set; }
        public Func<T> GetterFuncHandler { get; set; }       
        public Func<T,V> MakerFuncHandler { get; set; }    
      
        public T DoGetterFuncHandler()
        {           
            return this.GetterFuncHandler.Invoke();
        }
        public V DoMakerFuncHandler(T input)
        {
           return this.MakerFuncHandler.Invoke(input);
        }
    }
}
