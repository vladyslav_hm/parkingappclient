﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ParkingAppClient.Controllers
{
   public class ParkingController
    {
        public string BaseUrl { get; set; } = "https://localhost:5001/api/parking";
        private readonly HttpClient _httpClient;

        public ParkingController()
        {
            _httpClient = new HttpClient(); 
        }

        public string GetBalance()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/Balance").Result;
        }

        /// <summary>
        /// Сумму заработанных денег за последнюю минуту.
        /// </summary>
        /// <returns></returns>
        public string GetProfit()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/profit").Result;
        }

        /// <summary>
        /// Узнать количество свободных/занятых мест на парковке.
        /// </summary>
        /// <returns></returns>
        public string GetPlaceStatus()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/placeStatus").Result;
        }

        /// <summary>
        /// Вывести на экран все Транзакции Парковки за последнюю минуту
        /// </summary>
        /// <returns></returns>
        public string GetTransactions()
        {
       
            return _httpClient.GetStringAsync($"{BaseUrl}/transactions").Result;
        }

        /// <summary>
        /// Вывести всю историю Транзакций (считав данные из файла Transactions.log)
        /// </summary>
        /// <returns></returns>
        public string GetTransactionsHistory()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/transactionsHistory").Result;
        }


        /// <summary>
        /// Вывести на экран список всех Транспортных средств
        /// </summary>
        /// <returns></returns>
        public string GetAllVehicleOnParking()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/vehicleOnParking").Result;
        }

        /// <summary>
        /// Создать/поставить Транспортное средство на Парковку
        /// </summary>
        /// <param name="vehicleType"></param>
        public string AddVehicleInParking(string vehicleType)
        {
            return _httpClient.PutAsync($"{BaseUrl}/addVehicle/{vehicleType}", new StringContent(vehicleType)).Result.Content.ReadAsStringAsync().Result;        
        }

        /// <summary>
        /// Пополнить баланс конкретного Транспортного средства
        /// </summary>
        /// <param name="vehicleId_Funds"></param>
        public string TopUpVehicleBalance(string vehicleId_Funds)
        {
            string[] _params = vehicleId_Funds.Split(";");
            if(Guid.TryParse(_params[0], out Guid guid) && float.TryParse(_params[1],  out float funds))
            {
                return _httpClient
                    .PostAsync($"{BaseUrl}/vehicleOnParking/{funds}",
                        new StringContent(guid.ToString())).Result.Content.ReadAsStringAsync().Result;
            }

            return null;
        }

        /// <summary>
        /// Удалить/забрать Транспортное средство с Парковки
        /// </summary>
        /// <param name="vehicleBaseModelId"></param>
        public string DeleteVehicleFromParking(string vehicleBaseModelId)
        {
            return _httpClient.DeleteAsync($"{BaseUrl}/deleteVehicle/{vehicleBaseModelId}").Result.Content.ReadAsStringAsync().Result;
        }

        public string GetAllAvailableVehicle()
        {
            return _httpClient.GetStringAsync($"{BaseUrl}/availableVehicle").Result;                   
        }
    }
}

